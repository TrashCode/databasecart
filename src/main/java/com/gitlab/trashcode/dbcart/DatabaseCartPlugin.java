package com.gitlab.trashcode.dbcart;

import com.gitlab.sm1le.api.query.QueryBuilder;
import com.gitlab.sm1le.sql.connection.AbstractConnection;
import com.gitlab.sm1le.sql.connection.MySQLConnection;
import com.gitlab.sm1le.util.Messenger;
import com.gitlab.trashcode.dbcart.api.CartUserManager;
import com.gitlab.trashcode.dbcart.command.DatabaseCartCommand;
import org.bukkit.plugin.java.JavaPlugin;

public class DatabaseCartPlugin extends JavaPlugin {

    private AbstractConnection mainConnection;
    private CartUserManager manager;
    private Messenger messenger;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        this.mainConnection = new MySQLConnection(getConfig().getString("settings.mysql.host"), getConfig().getString("settings.mysql.database"), getConfig().getString("settings.mysql.user"), getConfig().getString("settings.mysql.password"));
        QueryBuilder.create(mainConnection.getDataSource()).query("CREATE TABLE IF NOT EXISTS `{0}` (`SERVER` VARCHAR, `TYPE` VARCHAR, `PLAYER` VARCHAR, `COMMAND` VARCHAR)", getConfig().getString("settings.mysql.table")).execute();
        this.manager = new CartUserManager(mainConnection, getConfig());
        this.messenger = new Messenger(getConfig().getConfigurationSection("messages"));
        getCommand("databasecart").setExecutor(new DatabaseCartCommand(this));
    }

    @Override
    public void onDisable() {
        mainConnection.close();
    }

    public Messenger getMessenger() {
        return messenger;
    }

    public CartUserManager getManager() {
        return manager;
    }

    public void reconnect() {
        this.mainConnection = new MySQLConnection(getConfig().getString("settings.mysql.host"), getConfig().getString("settings.mysql.database"), getConfig().getString("settings.mysql.user"), getConfig().getString("settings.mysql.password"));
    }
}
