package com.gitlab.trashcode.dbcart.api;

import com.gitlab.sm1le.api.query.QueryBuilder;
import com.gitlab.sm1le.sql.connection.AbstractConnection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;
import java.util.stream.Collectors;

public class CartUser {

    private String name;
    private AbstractConnection connection;
    private FileConfiguration mainConfig;

    CartUser(String name, AbstractConnection connection, FileConfiguration mainConfig) {
        this.name = name;
        this.connection = connection;
        this.mainConfig = mainConfig;
    }

    public String getName() {
        return name;
    }

    public List<String> getPurchasedCommands(String type) {
        return QueryBuilder.create(connection.getDataSource())
                .selectQuery("SELECT `COMMAND` FROM `{0}` WHERE `SERVER` = '{1}' AND (`TYPE` = '{2}' OR `TYPE` = 'ALL') AND `PLAYER` = '{3}'", mainConfig.getString("settings.mysql.table"), mainConfig.getString("settings.server"), type, name)
                .execute()
                .getRows().stream()
                .map(row -> row.getValue("COMMAND", String.class))
                .collect(Collectors.toList());
    }

    public void reset(String type) {
        QueryBuilder.create(connection.getDataSource()).query("DELETE FROM `{0}` WHERE `SERVER` = '{1}' AND (`TYPE` = '{2}' OR `TYPE` = 'ALL') AND `PLAYER` = '{3}'", mainConfig.getString("settings.mysql.table"), mainConfig.getString("settings.server"), type, name).execute();
    }

}
