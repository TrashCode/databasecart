package com.gitlab.trashcode.dbcart.api;

import com.gitlab.sm1le.sql.connection.AbstractConnection;
import org.bukkit.configuration.file.FileConfiguration;

import java.sql.Connection;
import java.sql.SQLException;

public class CartUserManager {

    private AbstractConnection connection;
    private FileConfiguration mainConfig;

    public CartUserManager(AbstractConnection connection, FileConfiguration mainConfig) {
        this.connection = connection;
        this.mainConfig = mainConfig;
    }

    public CartUser getUser(String name) {
        return new CartUser(name, connection, mainConfig);
    }

}
